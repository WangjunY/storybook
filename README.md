# Linnworks Design System Storybook

A storybook for components documentation.

## Environment

Angular 11.1.2  
Storybook 6.1.20  
Typescript 4.0.2

## Installation

```
npm install
```

## Running Storybook

```
npm run storybook
```

## Storybook Build

```
npm run build-storybook
```
