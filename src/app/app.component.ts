import {AfterViewInit, Component, ElementRef, Inject, Input, OnInit, TemplateRef, ViewChild} from "@angular/core";
import {ErrorStateMatcher} from "@angular/material/core";
import {FormControl, FormGroupDirective, NgForm, Validators} from "@angular/forms";
import {MatAutocomplete} from "@angular/material/autocomplete";
import {CdkScrollable, ScrollDispatcher} from "@angular/cdk/overlay";
import {MatButton} from "@angular/material/button";
import {CdkDragDrop, moveItemInArray} from "@angular/cdk/drag-drop";
import {MatDialog} from "@angular/material/dialog";
import {MatDrawerContent} from "@angular/material/sidenav";
import {
  SnackBarService,
  ToastMessageType,
  NavItem,
  SidePanelAction
} from "@linnworks/design-system";
import {PageEvent} from '@angular/material/paginator';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return true;
  }
}

export class SelectErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return control.errors && control.errors["manualErrorTrigger"];
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'linnworks-storybook';

  @ViewChild(MatDrawerContent) drawerContent;
  @ViewChild("backToTopButton") backToTopButton: MatButton;

  favoriteSeason: string;
  seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];

  label = "Label";
  inputValue = "input value";
  errorInput = "Error value";

  errorStateMatcher = new MyErrorStateMatcher();

  visible = true;
  selectable = true;
  removable = true;

  @ViewChild('fruitInput') fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  @Input()
  private someData: number;

  columnDefs = [
    {field: 'make'},
    {field: 'model'},
    {field: 'price'}
  ];

  rowData = [
    {make: 'Toyota', model: 'Celica', price: 35000},
    {make: 'Ford', model: 'Mondeo', price: 32000},
    {make: 'Porsche', model: 'Boxter', price: 72000}
  ];
  toggleValue: boolean = true;
  checked: boolean = true;
  checkedIndeterminate: boolean = true;
  errorExampleFormControl: FormControl = new FormControl('test', [
    Validators.required,
    Validators.email,
  ]);

  backToTop: boolean = false;
  hidden: boolean = false;
  badgeTypeSelect: FormControl = new FormControl("primary");

  navItems: NavItem[] = [
    {
      disabled: true,
      displayName: "Disabled item",
      iconName: "fa-trash",
      notificationsCount: 1
    },
    {
      disabled: false,
      displayName: "Users",
      iconName: "fa-user",
      notificationsCount: 99,
      warn: true,
      children: [
        {
          displayName: 'Child item #1',
          iconName: 'fa-pencil',
          warn: true
        },
        {
          displayName: 'Child item #2',
          iconName: 'fa-pencil',
          notificationsCount: 88
        },
        {
          displayName: 'Child item #3',
          iconName: 'fa-pencil',
          warn: true
        }
      ]
    }
  ];


  typesOfShoes: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];

  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  toppingsControl = new FormControl();


  links = ['First', 'Second', 'Third'];
  activeLink = this.links[0];

  tabs = ['First', 'Second', 'Third'];
  uploadUrl: string;

  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  pageEvent: PageEvent;

  constructor(private scrollDispatcher: ScrollDispatcher,
              private dialog: MatDialog,
              private _snackBar: SnackBarService) {
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }

  addLink() {
    this.links.push(`Link ${this.links.length + 1}`);
  }

  addTab() {
    this.tabs.push('New');
  }

  removeTab(index: number) {
    this.tabs.splice(index, 1);
  }

  openSuccessMessage() {
    this._snackBar.openSuccessMessage('Success: This is a success message')
  }

  openErrorMessage() {
    this._snackBar.openErrorMessage('Error: This is a error message')
  }

  openWarnMessage() {
    this._snackBar.openWarningMessage('Warn: This is a warn message')
  }

  openInfoMessage() {
    this._snackBar.openInfoMessage('Info: This is a info message')
  }

  openSnackBar() {
    this._snackBar.openToastMessage({
      data: {
        message: 'Success: This is a success message',
        type: ToastMessageType.Success,
        linkText: 'test',
        linkUrl: 'https://www.linnworks.com/',
        isHighPriority: true
      }
    });
  }

  openSnackBar1() {
    this._snackBar.openToastMessage({
      data: {
        message: 'Info: This is a info message',
        type: ToastMessageType.Info,
        linkText: 'test',
        linkUrl: 'https://www.linnworks.com/',
        isHighPriority: true
      }
    });
  }

  openSnackBar2() {
    this._snackBar.openToastMessage({
      data: {
        message: 'Error: This is a error message',
        type: ToastMessageType.Error,
        linkText: 'test',
        linkUrl: 'https://www.linnworks.com/',
        isHighPriority: true
      }
    });
  }

  openSnackBar3() {
    this._snackBar.openToastMessage({
      data: {
        message: 'Warning: This is a warning message',
        type: ToastMessageType.Warn,
        linkText: 'test',
        linkUrl: 'https://www.linnworks.com/',
      }
    });
  }

  ngOnInit(): void {
    console.log("resolved data: 'someData' is ", this.someData);
  }

  onSlideChange(event) {
    console.log(event);
  }

  onToggleChange(event) {
    console.log(event);
  }

  ngAfterViewInit(): void {
    this.scrollDispatcher.scrolled(100).subscribe((scrollable: CdkScrollable) => {
      if (!scrollable) return;

      const top = scrollable.measureScrollOffset("top");

      this.backToTop = top > 100;

      if (this.backToTop) {
        this.backToTopButton._elementRef.nativeElement.classList.remove("hidden-button");
      } else {
        this.backToTopButton._elementRef.nativeElement.classList.add("hidden-button");
      }
    });
  }

  scrollTop() {
    const div = this.drawerContent.elementRef.nativeElement.querySelector("div.well");

    div.scrollTo({
      behavior: "smooth",
      top: 0
    })
  }

  scroll(id: string) {
    const navigateTo = this.drawerContent.elementRef.nativeElement.querySelector(id) as HTMLElement;
    const parentDiv = this.drawerContent.elementRef.nativeElement.querySelector("div.well");

    parentDiv.scrollTo({
      behavior: "smooth",
      top: navigateTo.getBoundingClientRect().y - navigateTo.clientHeight - 40
    });
  }

  changeView(item: NavItem) {
    console.log("you can change view when navigation worked", item);
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.typesOfShoes, event.previousIndex, event.currentIndex);
  }

  @ViewChild("mediumDialogTemplate")
  mediumDialogTemplate: TemplateRef<any>;

  @ViewChild("fixedHeightDialogTemplate")
  fixedHeightDialogTemplate: TemplateRef<any>;

  @ViewChild("warningDialogTemplate")
  warningDialogTemplate: TemplateRef<any>;

  sidePanelActions: SidePanelAction[] = [
    {
      actionFn() {
        console.log("action clicked");
      },
      text: "Test action"
    }
  ];

  openMediumDialog() {
    this.dialog.open(this.mediumDialogTemplate);
  }

  openFixedHeightDialog() {
    this.dialog.open(this.fixedHeightDialogTemplate);
  }

  openWarningDialog() {
    this.dialog.open(this.warningDialogTemplate);
  }

  modalResized(isExpanded: boolean) {
    console.log("resize: ", isExpanded);
  }

}
