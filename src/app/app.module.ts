import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {DesignSystemModule} from "@linnworks/design-system";
import {SnackBarService} from "@linnworks/design-system";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AppComponent} from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DesignSystemModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [SnackBarService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
