import {moduleMetadata} from '@storybook/angular';
import {Story} from '@storybook/angular/types-6-0';
import {MatCheckbox, MatCheckboxModule} from '@angular/material/checkbox';
import {action} from '@storybook/addon-actions';
import {withKnobs, boolean, select, text} from '@storybook/addon-knobs';
import {DesignSystemModule} from "@linnworks/design-system";

export default {
  title: 'Components/Checkbox',
  component: MatCheckbox,
  decorators: [
    moduleMetadata({
      imports: [MatCheckboxModule, DesignSystemModule.forRoot()],
    }),
    withKnobs,
  ],
};

const Template: Story<MatCheckbox> = () => ({
  component: MatCheckbox,
  props: {
    disabled: boolean('disabled', false),
    checked: boolean('checked', false),
    indeterminate: boolean('indeterminate', false),
    label: text('label', 'Check me'),
    labelPosition: select('labelPosition', ['after', 'before'], 'after'),
    onIndeterminateChange: action('indeterminateChange'),
    onChange: action('change')
  },
  template: `
    <div>
      <mat-checkbox [disabled]="disabled"
       [checked]="checked"
       [indeterminate]="indeterminate"
       [labelPosition]="labelPosition"
       (indeterminateChange)="onIndeterminateChange($event)"
       (change)="onChange($event)">
        {{label}}
      </mat-checkbox>
    </div>
    `,
});
export const Checkbox = Template.bind({});

