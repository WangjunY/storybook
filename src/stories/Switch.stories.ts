import {moduleMetadata} from '@storybook/angular';
import {Story} from '@storybook/angular/types-6-0';
import {action} from '@storybook/addon-actions';
import {withKnobs, boolean, select, text} from '@storybook/addon-knobs';
import {DesignSystemModule} from "@linnworks/design-system";
import {MatSlideToggle, MatSlideToggleModule} from "@angular/material/slide-toggle";

export default {
  title: 'Components/Switch',
  component: MatSlideToggle,
  decorators: [
    moduleMetadata({
      imports: [MatSlideToggleModule, DesignSystemModule.forRoot()],
    }),
    withKnobs,
  ],
};

const Template: Story<MatSlideToggle> = () => ({
  component: MatSlideToggle,
  props: {
    disabled: boolean('disabled', false),
    checked: boolean('checked', true),
    labelPosition: select('labelPosition', ['after', 'before'], 'after'),
    onToggleChange: action('toggleChange')
  },
  template: `
    <div>
      <mat-slide-toggle
       [disabled]="disabled"
       [checked]="checked"
       [labelPosition]="labelPosition"
       (toggleChange)="onToggleChange($event)">
         Primary
      </mat-slide-toggle>
    </div>
    `,
});
export const Switch = Template.bind({});

