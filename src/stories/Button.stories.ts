import {MatButton, MatButtonModule} from "@angular/material/button";
import {DesignSystemModule} from "@linnworks/design-system";
import {moduleMetadata} from '@storybook/angular';
import {withKnobs, boolean, select} from '@storybook/addon-knobs';
import {Meta, Story} from "@storybook/angular/types-6-0";
import {withDesign} from 'storybook-addon-designs'

export default {
  title: 'Components/Button',
  component: MatButton,
  decorators: [
    moduleMetadata({
      imports: [MatButtonModule, DesignSystemModule.forRoot()],
    }),
    withKnobs,
    withDesign
  ],
} as Meta;

const Template1: Story<MatButton> = () => ({
  component: MatButton,
  props: {
    color: select('color', ['primary', 'tertiary', 'warn'], 'primary'),
    disabled: boolean('disabled', false),
  },
  template: `
    <p>
      <button mat-flat-button [color]="color" [disabled]="disabled"><em class="fa fa-plus"></em> Add item</button>
      <button mat-flat-button [color]="color" disabled>Disabled</button>
      <button mat-flat-button [color]="color" [disabled]="disabled">Large</button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="medium">Medium</button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="small">Small</button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="small"><em class="fa fa-plus"></em> Add item</button>
    </p>
    `,
});

const Template2: Story<MatButton> = () => ({
  component: MatButton,
  props: {
    disabled: boolean('disabled', false),
  },
  template: `
    <p>
       <button mat-flat-button [disabled]="disabled"><em class="fa fa-backward"></em> Go back</button>
       <button mat-flat-button disabled>Disabled</button>
       <button mat-flat-button [disabled]="disabled">Large</button>
       <button mat-flat-button [disabled]="disabled" class="medium">Medium</button>
       <button mat-flat-button [disabled]="disabled" class="small">Small</button>
       <button mat-flat-button [disabled]="disabled" class="small"><em class="fa fa-backward"></em> Go back</button>
    </p>
    `,
});

const Template3: Story<MatButton> = () => ({
  component: MatButton,
  props: {
    disabled: boolean('disabled', false),
  },
  template: `
    <p>
      <button mat-flat-button [disabled]="disabled" class="secondary"><em class="fa fa-ellipsis-v"></em> Actions</button>
      <button mat-flat-button [disabled]="disabled" class="secondary" disabled>Disabled</button>
      <button mat-flat-button [disabled]="disabled" class="secondary">Large</button>
      <button mat-flat-button [disabled]="disabled" class="medium secondary">Medium</button>
      <button mat-flat-button [disabled]="disabled" class="small secondary">Small</button>
      <button mat-flat-button [disabled]="disabled" class="small secondary"><em class="fa fa-ellipsis-v"></em> Actions</button>
    </p>
    `,
});

const Template4: Story<MatButton> = () => ({
  component: MatButton,
  props: {
    color: select('color', ['primary', 'tertiary', 'warn'], 'tertiary'),
    disabled: boolean('disabled', false),
  },
  template: `
    <p>
      <button mat-flat-button [color]="color" [disabled]="disabled"><em class="fa fa-minus"></em> Cancel</button>
      <button mat-flat-button disabled [color]="'tertiary'">Disabled</button>
      <button mat-flat-button [color]="color" [disabled]="disabled">Large</button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="medium">Medium</button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="small">Small</button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="small"><em class="fa fa-minus"></em> Cancel</button>
    </p>
    `,
});

const Template5: Story<MatButton> = () => ({
  component: MatButton,
  props: {
    color: select('color', ['primary', 'tertiary', 'warn'], 'warn'),
    disabled: boolean('disabled', false),
  },
  template: `
    <p>
      <button mat-flat-button [color]="color" [disabled]="disabled"><em class="fa fa-trash"></em> Delete</button>
      <button mat-flat-button [color]="color" [disabled]="disabled">Delete <em class="fa fa-trash end"></em></button>
      <button mat-flat-button [color]="color" disabled>Disabled</button>
      <button mat-flat-button [color]="color" [disabled]="disabled">Large</button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="medium">Medium</button>
    </p>
    <p>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="medium"><em class="fa fa-trash"></em> Medium</button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="medium">Medium <em class="fa fa-trash end"></em></button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="small">Small</button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="small"><em class="fa fa-trash"></em> Delete</button>
      <button mat-flat-button [color]="color" [disabled]="disabled" class="small">Delete <em class="fa fa-trash end"></em></button>
    </p>
    `,
});

const Template6: Story<MatButton> = () => ({
  component: MatButton,
  template: `
    <p>
      <button mat-fab [color]="'primary'"><em class="fa fa-trash"></em></button>
      <button mat-fab [color]="'warn'"><em class="fa fa-trash"></em></button>
      <button mat-fab><em class="fa fa-trash"></em></button>
      <button mat-fab [color]="'tertiary'"><em class="fa fa-trash"></em></button>
      <button mat-mini-fab [color]="'warn'"><em class="fa fa-trash"></em></button>
    </p>
    `,
});

const Template7: Story<MatButton> = () => ({
  component: MatButton,
  template: `
    <p>
      <button disabled mat-fab [color]="'primary'"><em class="fa fa-trash"></em></button>
      <button disabled mat-fab [color]="'warn'"><em class="fa fa-trash"></em></button>
      <button disabled mat-fab><em class="fa fa-trash"></em></button>
      <button disabled mat-fab [color]="'tertiary'"><em class="fa fa-trash"></em></button>
    </p>
    `,
});

export const Primary = Template1.bind({});
export const Secondary_Borderless = Template2.bind({});
export const Secondary = Template3.bind({});
export const Tertiary = Template4.bind({});
export const Warn = Template5.bind({});
export const Icon_buttons = Template6.bind({});
export const Disabled_Icon = Template7.bind({});

Template1.parameters = {
  design: {
    type: 'figma',
    url: 'https://www.figma.com/file/XosHGTvaiSrjSpLlvrIb6B/Linnworks.net-components?node-id=21%3A4',
  },
}

