import {moduleMetadata} from '@storybook/angular';
import {Story} from '@storybook/angular/types-6-0';
import {action} from '@storybook/addon-actions';
import {withKnobs} from '@storybook/addon-knobs';
import {
  DesignSystemModule,
  NavItem,
  SideNavHeaderComponent,
  LwSidenavDirective,
  BadgeComponent,
  MenuListItemComponent,
  SidenavService
} from "@linnworks/design-system";
import {MatSidenav, MatSidenavContainer, MatSidenavContent, MatSidenavModule} from "@angular/material/sidenav";
import {MatDivider} from "@angular/material/divider";
import {MatNavList} from "@angular/material/list";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

const navItems: NavItem[] = [
  {
    disabled: true,
    displayName: "Disabled item",
    iconName: "fa-trash",
    notificationsCount: 1
  },
  {
    disabled: false,
    displayName: "Users",
    iconName: "fa-user",
    notificationsCount: 99,
    warn: true,
    children: [
      {
        displayName: 'Child item #1',
        iconName: 'fa-pencil',
        warn: true
      },
      {
        displayName: 'Child item #2',
        iconName: 'fa-pencil',
        notificationsCount: 88
      },
      {
        displayName: 'Child item #3',
        iconName: 'fa-pencil',
        warn: true
      }
    ]
  }
];

export default {
  title: 'Components/Navigation',
  component: MatSidenavContainer,
  subcomponents: {
    MatSidenav,
    SideNavHeaderComponent,
    LwSidenavDirective,
    BadgeComponent,
    MatDivider,
    MatNavList,
    MenuListItemComponent,
    MatSidenavContent
  },
  decorators: [
    moduleMetadata({
      imports: [MatSidenavModule, DesignSystemModule.forRoot(), BrowserAnimationsModule],
      providers: [SidenavService]
    }),
    withKnobs,
  ],
};

const Template: Story<MatSidenavContainer> = (args: MatSidenavContainer) => ({
  component: MatSidenavContainer,
  props: args,
  template: `
    <div>
      <mat-sidenav-container class="example-side-nav">
        <mat-sidenav lwSidenav (itemActivated)="changeView($event);">
          <side-nav-header [title]="'User Management'">
            <badge color="accent"><em class="fa fa-check"></em> Paid</badge>
          </side-nav-header>
          <mat-divider></mat-divider>
          <div>
            <mat-nav-list>
             <menu-list-item *ngFor="let item of navItems" [item]="item"></menu-list-item>
            </mat-nav-list>
          </div>
        </mat-sidenav>
        <mat-sidenav-content>
          <p>
            Navigation content
          </p>
        </mat-sidenav-content>
      </mat-sidenav-container>
    </div>
    `,
});

export const SecondaryNavigation = Template.bind({});

SecondaryNavigation.args = {
  navItems: navItems,
  changeView: action('itemActivated')
}



